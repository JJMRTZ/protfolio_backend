from django.contrib import admin
from .models import Proyects
# Register your models here.
class ProyectAdmin(admin.ModelAdmin):
    readonly_fields = ('created', 'updated')



admin.site.register(Proyects, ProyectAdmin)
