from .models import Proyects
from .serializer import ProyectSerializer
from rest_framework import viewsets

# Create your views here.
class ProyectsViewSet(viewsets.ModelViewSet):
     serializer_class = ProyectSerializer
     def get_queryset(self):
          queryset = Proyects.objects.order_by('title')
          title = self.request.query_params.get('title', None)
          if title is not None:
            queryset = queryset.filter(title=title)
          return queryset
     