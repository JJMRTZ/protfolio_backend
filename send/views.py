from rest_framework.views import APIView
from django.core.mail import send_mail
from rest_framework import permissions
from django.conf import settings
from django.utils.decorators import method_decorator
from django.http import HttpResponse,JsonResponse
from django.views.decorators.csrf import  csrf_protect,csrf_exempt,ensure_csrf_cookie
# Create your views here.
@method_decorator(ensure_csrf_cookie,name='dispatch')
class mailView(APIView):
    permission_classes =(permissions.AllowAny, )
    def post(self,request, form=None):
            data = self.request.data
            subject = data['name']
            message = data['email']+': '+data['message']
            print(settings.EMAIL_HOST_USER)
            try:
                send_mail(
                subject,
                message,
                settings.EMAIL_HOST_USER,
                ['juannadador21@gmail.com'],
                fail_silently=False 
                )
            except :
                return HttpResponse(status=201)
            return HttpResponse("Message send, i'll contact you soon as posible")
