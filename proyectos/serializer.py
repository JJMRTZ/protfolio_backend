from rest_framework import serializers
from .models import Proyects

class ProyectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Proyects
        fields = '__all__'