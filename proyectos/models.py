from django.db import models
from django.contrib.postgres.fields import ArrayField
# Create your models here.
class Proyects(models.Model):
    title = models.CharField(max_length=250)
    images = ArrayField(models.TextField())
    description = models.TextField( blank=True,null=True)
    url = models.URLField(max_length=200, blank=True,null=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title